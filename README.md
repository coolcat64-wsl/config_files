# README #

This project contains configuration files stored in /home/username for vim,
     tmux, and bash applications.  It also contains /etc/ configuration files
     and bash scripts requiring WSL 2.  There are a few Windows PowerShell
     scripts thrown in for good measure.

### What is this repository for? ###

* Download these configuration files and try them out. These are customized for a WSL 2 Debian based distros.
* Version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Download and modify as you need to!
* Configuration: All the .*rc config files are for the /home/username directory.
   The settings.json file is for Windows Terminal.  That is placed in the
   %LOCALAPPDATA%\Packages\WINDOWS_TERMINAL_FOLDER\LocalState\ directory. Some
   examples of WINDOWS_TERMINAL_FOLDERs are:
      Microsoft.WindowsTerminal_8wekyb3d8bbwe
      Microsoft.WindowsTerminal_7923nhf822j37
   The folder name depends on your particular installation.
* Dependencies: install bash version 5.0.17, Vim version 8.1, and tmux version
   3.0a in WSL 2. The settings.json configuration supports Windows Terminal
   version 1.3.2651.0.
* Database configuration: None
* How to run tests: None
* Deployment instructions: None

### How do I setup a GUI in WSL? ###

[Adapted from here](https://dev.to/darksmile92/linux-on-windows-wsl-with-desktop-environment-via-rdp-522g)

In Ubuntu WSL:
sudo apt-get purge xrdp

then

sudo apt-get install xrdp
sudo apt-get install xfce4
sudo apt-get install xfce4-goodies

configure (use the etc/xrdp/xrdp.ini from this folder, or):
sudo cp /etc/xrdp/xrdp.ini /etc/xrdp/xrdp.ini.bak
sudo sed -i 's/3389/3390/g' /etc/xrdp/xrdp.ini
sudo sed -i 's/max_bpp=32/#max_bpp=32\nmax_bpp=128/g' /etc/xrdp/xrdp.ini
sudo sed -i 's/xserverbpp=24/#xserverbpp=24\nxserverbpp=128/g' /etc/xrdp/xrdp.ini

THIS IS IMPORTANT:
echo xfce4-session > ~/.xsession
NOTE: Use the included .xsession file

sudo /etc/init.d/xrdp start

Now in Windows, use Remote Desktop Connection
localhost:3390
then login with Xorg, fill in your username and password.

### How do I keep the screensaver from activating in WSL? ###

In the xfce4 power manager settings, set the screen save and lock to happen NEVER.

Add the following line to the .xsession file:
xset s off
NOTE: Use the included .xsession file

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: mdecler2@gmail.com
* Other community or team contact
