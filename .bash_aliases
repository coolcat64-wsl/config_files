# ~/.bash_aliases: executed by bash(1) for non-login shells via the .bashrc.
# Alias definitions.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

# File overwrite confirmation aliases
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

# Base command for mounting external Windows drives
# Removed options ,umask=22,fmask=11
mnt_win='sudo mount -t drvfs -o metadata,case=dir'

# Print to terminal and mount D:\ in WSL
mnt_d="${mnt_win} D: /mnt/d/)"
alias mount-d='echo $mnt_d;$mnt_d'

# Print to terminal and mount E:\ in WSL
mnt_e="${mnt_win} E: /mnt/e/"
alias mount-e='echo $mnt_e;$mnt_e'

# Print to terminal and mount F:\ in WSL
mnt_f="${mnt_win} F: /mnt/f/"
alias mount-f='echo $mnt_f;$mnt_f'

# Print to terminal and mount G:\ in WSL
mnt_g="${mnt_win} G: /mnt/g/"
alias mount-g='echo $mnt_g;$mnt_g'

# Print to terminal and mount H:\ in WSL
mnt_h="${mnt_win} H: /mnt/h/"
alias mount-h='echo $mnt_h;$mnt_h'

# Alias vim quit command with exit
alias ':q'='exit'
