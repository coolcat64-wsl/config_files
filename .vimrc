" Michael's .vimrc file built for Linux Debian!
" Modifcation of Keith Amling's .vimrc
" (double quotes are comments)

" Mouse support! Heresy!
set mouse=a

set background=dark
set backspace=2
set cindent
set clipboard=unnamed
set cpoptions+=$
set expandtab
set ignorecase
set incsearch
set laststatus=2
set nobackup
set noerrorbells
set noesckeys
set hlsearch
set nowrap
"set number
set ruler
set shiftwidth=3
set shortmess+=I
set showcmd
set showmode
set smartcase
set t_vb=

" Set the terminal title while editing
set title
" Set the terminal title to machine name upon exit
execute "set titleold=".hostname()

set ts=3
"set tw=80
set visualbell
set whichwrap=b,s

"set wildmode=longest,list
set wildmenu
set wildmode=list:longest,full

set writebackup
" Restore last position of the cursor
set hidden
"set viminfo

filetype plugin on
filetype indent on
syntax on

" WSL yank support...requires C:\ to be mounted with execution privilege
let s:clip = '/mnt/c/Windows/System32/clip.exe'  " change this path according to your mount point
if executable(s:clip)
   augroup WSLYank
      autocmd!
      autocmd TextYankPost * if v:event.operator ==# 'y' | call system(s:clip, @0) | endif
   augroup END
endif

" NERDTree like settings for netrw!
" Adapted from: https://shapeshed.com/vim-netrw/
" DONT USE THIS! The NERDTree plugin is much better.
"let g:netrw_banner = 0
"let g:netrw_liststyle = 3
"let g:netrw_browse_split = 4
"let g:netrw_altv = 1
"let g:netrw_winsize = 25
"augroup ProjectDrawer
"   autocmd!
"   autocmd VimEnter * :Vexplore
"augroup END

" Folding options by Keith Amling
fu! SynFolds()
   set foldmethod=syntax
   set foldcolumn=5
   syn region myFold start="{" end="}" transparent fold
endf

"hi FoldColumn ctermbg=darkblue ctermfg=white
"hi! Comment term=bold ctermfg=cyan

" ELF file edit script fu by Keith Amling
fu! OnEditELF()
	set foldmethod=manual
	set foldcolumn=5
	%!readelf -a "<afile>:p"; echo XXXXXX; objdump --source
	"<afile>:p"
	:1,/^XXXXXX$/-fo
	" :1foldopen
	:/^XXXXXX$/+,$fo
	:$foldopen
	:/^XXXXXX$/d
	:1
	set buftype=nofile
endf

fu! OnEditFile()
	syn sync fromstart
	set foldmethod=manual
	set foldlevel=99

	syn match EvilSpace " \+$" containedin=ALL
	hi link EvilSpace Error
	syn match EvilSpace2 "\t" containedin=ALL
	hi link EvilSpace2 Error
endf

" Move to last cursor position when buffer is read
au BufReadPost *
     \ if line("'\"") > 1 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif

" Buffer read by file type
au BufNewFile,BufNewFile *.def set filetype=cpp
"au BufNewFile,BufRead * silent call OnEditFile()
"au BufNewFile,BufRead *.c,*.C,*.h,*.cpp,*.pl silent call SynFolds()
"au BufNewFile,BufRead *.o,*.elf silent call OnEditELF()
